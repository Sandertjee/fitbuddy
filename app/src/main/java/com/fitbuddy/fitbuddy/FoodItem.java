package com.fitbuddy.fitbuddy;

import android.app.Activity;
import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

public class FoodItem {

    private static Map<ID, FoodItem> foodList;
    private static Map<ID, Bitmap> foodIcons;

    private ID id;
    private String foodName;
    private String foodDescription;
    private Categories foodCategory;
    private int foodCalories;
    private int foodPrice;
    private Bitmap foodImage;
    private int health_points;

    enum ID {
        None,
        Potato,
        Rice,
        Pasta,
        Fries,
        CheeseBurger,
        Pizza,
        ChickenWings,
        Apple,
        Banana,
        Pear,
        Kiwi,
        Melon,
        Tomato,
        Lettuce,
        Cucumber,
        Carrot,
        Brocolli,
        Ham,
        Steak,
        Sausage,
        ChickenBreast,
        Coke,
        RedBull,
        DietCoke,
        Water,
        Milk,
        IceTea,
        Coffee,
        MonsterEnergy
    }

    enum Categories {
        Grains,
        Meat,
        FastFood,
        Vegetables,
        Fruit,
        Drinks,
    }

    private FoodItem(ID id, String name, String description, Categories category , int price, int calories, int healthPoints) {
        this.id = id;
        this.foodName = name;
        this.foodDescription = description;
        this.foodCategory = category;
        this.foodPrice = price;
        this.foodCalories = calories;
        this.foodImage = getIcon(id);
        this.health_points = healthPoints;
    }

    public static void Init(Activity activity) {
        foodList = new HashMap<>();
        foodIcons = new HashMap<>();
        loadFoodIcons(activity);

        // Grains
        foodList.put(ID.Potato, new FoodItem(ID.Potato, "Potato", "200 Grams of potato happiness", Categories.Grains, 8, 146, -3));
        foodList.put(ID.Rice, new FoodItem(ID.Rice, "Rice", "Rice to the challenge", Categories.Grains, 10, 365, 2));
        foodList.put(ID.Pasta, new FoodItem(ID.Pasta, "Pasta", "The best italian food", Categories.Grains, 12, 350, -8));
        // FastFood
        foodList.put(ID.Fries, new FoodItem(ID.Fries, "Fries", "Hmm, you're loving it!", Categories.FastFood, 6, 660, -25));
        foodList.put(ID.CheeseBurger, new FoodItem(ID.CheeseBurger, "Cheeseburger", "Have it your way!", Categories.FastFood, 7, 300, -20));
        foodList.put(ID.Pizza, new FoodItem(ID.Pizza, "Pizza", "A slice of pure joy!", Categories.FastFood, 6, 117, -15));
        foodList.put(ID.ChickenWings, new FoodItem(ID.ChickenWings, "Fried Chicken Wings", "Don't let these two fly away", Categories.FastFood, 8, 178, -16));
        // Fruit
        foodList.put(ID.Apple, new FoodItem(ID.Apple, "Apple", "How you like them apples?", Categories.Fruit, 4, 65, 1));
        foodList.put(ID.Pear, new FoodItem(ID.Pear, "Pear", "Pear up friend!", Categories.Fruit, 5, 75, 1));
        foodList.put(ID.Banana, new FoodItem(ID.Banana, "Banana", "Banana whats my name, banana", Categories.Fruit, 4, 60, 1));
        foodList.put(ID.Kiwi, new FoodItem(ID.Kiwi, "Kiwi", "This Kiwi won't fly", Categories.Fruit, 3, 46, 1));
        foodList.put(ID.Melon, new FoodItem(ID.Melon, "Melon", "One in a Melon", Categories.Fruit, 3, 45, 1));
        // Vegetables
        foodList.put(ID.Tomato, new FoodItem(ID.Tomato, "Tomato", "Pure love from head tomatoes", Categories.Vegetables, 2, 25, 2));
        foodList.put(ID.Lettuce, new FoodItem(ID.Lettuce, "Lettuce", "Lettuce be free!", Categories.Vegetables, 2, 32, 5));
        foodList.put(ID.Cucumber, new FoodItem(ID.Cucumber, "Cucumber", "Just for eating, nothing else!", Categories.Vegetables, 3, 36, 2));
        foodList.put(ID.Carrot, new FoodItem(ID.Carrot, "Carrot", "I carrot about you!", Categories.Vegetables, 2, 25, 2));
        foodList.put(ID.Brocolli, new FoodItem(ID.Brocolli, "Brocolli", "I'm basically a small tree", Categories.Vegetables, 4, 69, 2));
        // Meat
        foodList.put(ID.Ham, new FoodItem(ID.Ham, "Ham", "Stop! it's Ham(mer)time", Categories.Meat, 12, 290, 10));
        foodList.put(ID.Steak, new FoodItem(ID.Steak, "Steak", "The steaks are high!", Categories.Meat, 15, 270, -9));
        foodList.put(ID.Sausage, new FoodItem(ID.Sausage, "Sausage", "This item is the wurst!", Categories.Meat, 14, 301, -9));
        foodList.put(ID.ChickenBreast, new FoodItem(ID.ChickenBreast, "Chicken Breast", "Not the silicone kind", Categories.Meat, 14, 296, 5));
        // Drinks
        foodList.put(ID.Coke, new FoodItem(ID.Coke, "Coke", "Not the sniffing kind!", Categories.Drinks, 8, 139, -12));
        foodList.put(ID.RedBull, new FoodItem(ID.RedBull, "Red Bull", "Makes you fly!", Categories.Drinks, 8, 110, -15));
        foodList.put(ID.DietCoke, new FoodItem(ID.DietCoke, "Diet coke", "Like coke, but more slim.", Categories.Drinks, 7, 1, 1));
        foodList.put(ID.Water, new FoodItem(ID.Water, "Water", "Water you gonna do without me?", Categories.Drinks, 4, 0, 0));
        foodList.put(ID.Milk, new FoodItem(ID.Milk, "Milk", "Straight from the cow", Categories.Drinks, 6, 136, 2));
        foodList.put(ID.IceTea, new FoodItem(ID.IceTea, "Ice Tea", "Not to be confused with the rapper, Ice-T", Categories.Drinks, 9, 185, -10));
        foodList.put(ID.Coffee, new FoodItem(ID.Coffee, "Coffee", "Don't confuse with Covfeve - Trump 2017", Categories.Drinks, 6, 3, 2));
        foodList.put(ID.MonsterEnergy, new FoodItem(ID.MonsterEnergy, "Monster Energy", "Does not actually contain monsters!", Categories.Drinks, 10, 210, -17));
    }

    private static void loadFoodIcons(Activity activity) {
        SpriteSheet food_sheet = new SpriteSheet(activity, R.drawable.food_spritesheet, 4, 8);
        foodIcons.put(ID.Potato, food_sheet.getSprite(1, 1));
        foodIcons.put(ID.Rice, food_sheet.getSprite(1, 2));
        foodIcons.put(ID.Pasta, food_sheet.getSprite(1, 3));
        foodIcons.put(ID.Fries, food_sheet.getSprite(1, 4));
        foodIcons.put(ID.ChickenWings, food_sheet.getSprite(1, 5));
        foodIcons.put(ID.Pizza, food_sheet.getSprite(1, 6));
        foodIcons.put(ID.CheeseBurger, food_sheet.getSprite(1, 7));
        foodIcons.put(ID.Apple, food_sheet.getSprite(1, 8));

        foodIcons.put(ID.Banana, food_sheet.getSprite(2, 1));
        foodIcons.put(ID.Pear, food_sheet.getSprite(2, 2));
        foodIcons.put(ID.Kiwi, food_sheet.getSprite(2, 3));
        foodIcons.put(ID.Melon, food_sheet.getSprite(2, 4));
        foodIcons.put(ID.Tomato, food_sheet.getSprite(2, 5));
        foodIcons.put(ID.MonsterEnergy, food_sheet.getSprite(2, 6));
        foodIcons.put(ID.IceTea, food_sheet.getSprite(2, 7));
        foodIcons.put(ID.RedBull, food_sheet.getSprite(2, 8));

        foodIcons.put(ID.Coffee, food_sheet.getSprite(3, 1));
        foodIcons.put(ID.Water, food_sheet.getSprite(3, 2));
        foodIcons.put(ID.Milk, food_sheet.getSprite(3, 3));
        foodIcons.put(ID.Coke, food_sheet.getSprite(3, 4));
        foodIcons.put(ID.DietCoke, food_sheet.getSprite(3, 5));
        foodIcons.put(ID.ChickenBreast, food_sheet.getSprite(3, 6));
        foodIcons.put(ID.Sausage, food_sheet.getSprite(3, 7));
        foodIcons.put(ID.Steak, food_sheet.getSprite(3, 8));

        foodIcons.put(ID.Brocolli, food_sheet.getSprite(4, 1));
        foodIcons.put(ID.Carrot, food_sheet.getSprite(4, 2));
        foodIcons.put(ID.Cucumber, food_sheet.getSprite(4, 3));
        foodIcons.put(ID.Lettuce, food_sheet.getSprite(4, 4));
        foodIcons.put(ID.Ham, food_sheet.getSprite(4, 5));
    }

    public static Bitmap getIcon(ID id) {
        return foodIcons.get(id);
    }

    public static FoodItem get(ID id) {
        return foodList.get(id);
    }

    public String getName() {
        return foodName;
    }

    public int getHealthPoints() {
        return health_points;
    }

    public String getDescription() {
        return foodDescription;
    }

    public Categories getCategory() {
        return foodCategory;
    }

    public int getPrice() {
        return foodPrice;
    }

    public Bitmap getImage(){ return foodImage; }

    public static Map<ID, FoodItem> getMap() {
        return foodList;
    }

}

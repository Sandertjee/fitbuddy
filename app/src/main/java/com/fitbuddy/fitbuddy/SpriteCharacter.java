package com.fitbuddy.fitbuddy;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SpriteCharacter {

    private int SPRITE_WALK_TIME = 0;
    private int BORDER_LEFT = 0;
    private int BORDER_RIGHT = 0;
    private int BOUNDARIES = 0;

    private Activity context;
    private Handler handler;
    private Visual current_visual;
    private State current_state = State.Normal;

    private String name;
    private Direction sprite_direction = Direction.Left;

    private FoodItem.ID current_food_item = FoodItem.ID.None;
    private Bitmap current_food_icon;

    private boolean running = false;
    private boolean is_dead = false;

    private int screen_width;
    private int screen_height;
    private int sprite_width;
    private int sprite_height;

    private boolean walking = false;
    private int step;

    private List<SpriteAction> actions;
    private int action_counter;
    private int current_action_index;
    private int position;

    enum Direction {Left, Right}
    enum State {Fat, Normal, Sick, Dead}
    enum Visual {FatDefault, FatWalk, FatEat, NormalDefault, NormalWalk, NormalEat, SickDefault, SickWalk, SickEat, Dead}
    enum Action {WalkLeft, WalkRight, StandStill, LookOpposite, Talk, EatFood, Die}

    private SpriteSheet sprite;
    private Map<Visual, Bitmap> sheet;

    private ImageView view;

    SpriteCharacter(Activity activity, int sprite_resource, String name){
        this.name = name;
        this.context = activity;
        this.sheet = new HashMap<>();
        sprite = new SpriteSheet(BitmapFactory.decodeResource(activity.getResources(), sprite_resource), 4, 3);
        initSpriteSheet();
        setScreenWidth();
    }

    SpriteCharacter(Activity activity, ImageView view, int sprite_resource, String name, State state, FoodItem.ID food_item){
        this(activity, view, sprite_resource, name, state, food_item, true);
    }

    SpriteCharacter(Activity activity, ImageView view, int sprite_resource, String name, State state, FoodItem.ID food_item, boolean pre_spawn){
        this.name = name;
        this.current_state = state;
        this.view = view;
        this.context = activity;
        this.sprite_direction = Direction.Left;
        this.current_visual = getStandingVisual(current_state);
        this.current_food_item = food_item;

        this.sprite = new SpriteSheet(BitmapFactory.decodeResource(activity.getResources(), sprite_resource), 4, 3);
        this.sheet = new HashMap<>();
        this.actions = new ArrayList<>();
        this.handler = new Handler();

        initSpriteSheet();
        setScreenWidth();
        standStill();

        if (pre_spawn) {
            if (food_item != FoodItem.ID.None){
                FoodItem.Init(activity);
                FoodItem item = FoodItem.get(food_item);
                current_food_icon = item.getImage();
                eatFood();
                spawnMiddle();
            } else {
                spawn();
            }
            start();
        }
    }

    private void start() {
        running = true;
        log("Sprite " + name + " started");
    }

    public void resume() {
        // running = true;
        log("Sprite " + name + " resumed");
//        go();
    }

    public void pause() {
        // running = false;
        log("Sprite " +
                name + " paused");
    }

    public void stop() {
        running = false;
        actions.clear();
        log("Sprite " + name + " stopped");
    }

    public void kill(){
        stop();
    }

    public boolean isRunning(){
        return running;
    }

    public void isDead(boolean bool) {
        this.is_dead = bool;
    }

    private void setPosition(int position) {
        this.position = position;
        view.setX(position);
    }

    private void spawn(){
        setPosition(screen_width + sprite_width + sprite_width);
        int pos =  new Random().nextInt(screen_width - sprite_width);
        doWalkTo(pos, 6000, Direction.Left);
    }

    public void spawnMiddle(){
        spawnMiddle(false);
    }

    public void spawnMiddle(boolean force){
        int pos =  new Random().nextInt(screen_width - sprite_width);
        setPosition(pos);
        Direction direction = (new Random().nextInt(3) + 1 > 1) ? Direction.Left : Direction.Right;
        if (!force) {
            doWalkTo(pos, 0, direction);
        } else {
            setDirection(direction);
        }
    }

    public int getScreenWidth(){ return screen_width; }

    public int getScreenHeight(){ return screen_height; }

    public int getWidth(){ return sprite_width; }

    public int getHeight(){ return sprite_height; }

    public String getDeathSentence(){
        String sentice;
        if (name != FitBuddy.Character.None.toString()){
            // 1 tm 6
            switch (new Random().nextInt(5) + 1){
                case 4:
                    sentice = name + "... why " + name + "...";
                    break;
                case 3:
                    sentice = name + " is in a better place now...";
                    break;
                case 2:
                    sentice = name + " has gone to the next world...";
                    break;
                default:
                    sentice = name + " died to soon...";
                    break;
            }
        } else {
            sentice = "Your buddy has died...";
        }
        return sentice;
    }

    private void setScreenWidth() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        screen_width = displaymetrics.widthPixels;
        screen_height = displaymetrics.heightPixels;
        log("Screen width: " + screen_width);

        sprite_width = sprite.getWidth();
        sprite_height = sprite.getHeight();
        log("Sprite width: " + sprite_width);

        step = (screen_width / sprite_width) / 2;
        log("Steps width: " + step);

        BOUNDARIES = (screen_width - sprite_width);
        log("Boundaries: " + BOUNDARIES);

        BORDER_RIGHT = screen_width - sprite_width;
        BORDER_LEFT = sprite_width / 2;
        log("Set borders: LEFT:" + BORDER_LEFT + " RIGHT:" + BORDER_RIGHT);

        SPRITE_WALK_TIME = (int) Math.floor(sprite_width * 0.75);
    }

    private void initSpriteSheet() {
        sheet.put(Visual.FatDefault, sprite.getSprite(1, 1));
        sheet.put(Visual.FatWalk, sprite.getSprite(1, 2));
        sheet.put(Visual.FatEat, sprite.getSprite(1, 3));
        sheet.put(Visual.NormalDefault, sprite.getSprite(2, 1));
        sheet.put(Visual.NormalWalk, sprite.getSprite(2, 2));
        sheet.put(Visual.NormalEat, sprite.getSprite(2, 3));
        sheet.put(Visual.SickDefault, sprite.getSprite(3, 1));
        sheet.put(Visual.SickWalk, sprite.getSprite(3, 2));
        sheet.put(Visual.SickEat, sprite.getSprite(3, 3));
        sheet.put(Visual.Dead, sprite.getSprite(4, 1));
    }

    public Bitmap getSprite(Visual visual){
        if (is_dead){
            current_visual = visual;
            return sheet.get(Visual.Dead);
        } else {
            current_visual = visual;
            return sheet.get(visual);
        }
    }

    public void setState(State state){
        current_state = state;
    }

    private int getSteps(int steps) {
        return (steps * step) * 20;
    }

    private void setDirection(Direction direction){
        sprite_direction = direction;
        view.setRotationY((direction == Direction.Left)? 0f : -180f);
    }

    private void next(){
        if (running){
            log("Current position: " + view.getX());
            position = (int) view.getX();
            current_action_index++;
            if (current_action_index < action_counter){
                SpriteAction objects = actions.get(current_action_index);
                final Action action = objects.getSpriteAction();
                final Integer time = objects.getValue();
                int duration = (time > 100)? 3500 : time;

                log("Performing " + action.toString() + ": " + current_action_index + " of " + action_counter);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doAction(action, time);
                    }
                }, (current_action_index == 1)? 500 : duration);
            } else {
                log("====== End of Actions ======");
            }
        } else {
            log("====== End of Run ======");
        }
    }

    public void go(){
        log(actions.toString());
        current_action_index = -1;
        next();
    }

    /*
    ===========================================================================================
    ACTIONS RAW
    ===========================================================================================
    */

    private void doAction(Action action, int integer){
        log("doAction: " + action);
        switch (action){
            case WalkLeft:
                doWalkLeft(integer);
                break;
            case WalkRight:
                doWalkRight(integer);
                break;
            case LookOpposite:
                doLookOpposite();
                break;
            case EatFood:
                doEat(current_food_item, integer);
                break;
            default:
            case StandStill:
                doStand();
                break;
        }
    }

    private void doLookOpposite(){
        Direction direction = (sprite_direction == Direction.Left)? Direction.Right : Direction.Left;
        log("Look " + direction);
        setDirection(direction);
        doStand();
    }

    private void doEat(FoodItem.ID food_item, int time){
        Bitmap bitmap = SpriteSheet.overlay(getSprite(getEatingVisual(current_state)), current_food_icon);

        if (current_food_item != FoodItem.ID.None){
            log(name + " is eating: " + food_item.toString());
            view.setImageBitmap(bitmap);
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                next();
            }
        }, time);
    }

    private void doStand(){
        Visual visual;
        switch (current_state){
            case Fat:
                visual = Visual.FatDefault;
                break;
            case Sick:
                visual = Visual.SickDefault;
                break;
            default:
            case Normal:
                visual = Visual.NormalDefault;
                break;
        }
        view.setImageBitmap(getSprite(visual));
        int time = (new Random().nextInt(5) + 1) * 500;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                next();
            }
        }, time);
    }

    public void died(){
        running = false;
        walking = false;
        view.setImageBitmap(getSprite(Visual.Dead));
    }

    private void doWalk(int steps, Direction direction) {
        boolean in_left_boundary = false;
        boolean in_right_boundary = false;
        if (position < BORDER_LEFT){
            direction = Direction.Right;
            in_left_boundary = true;
        } else if (position > BORDER_RIGHT){
            direction = Direction.Left;
            in_right_boundary = true;
        }

        int time = 2000;
        int distance = (direction == Direction.Left)? -getSteps(steps) : getSteps(steps);
        int go_to = (distance + position);

        String boundary;
        if (in_left_boundary){
            boundary = "left boundary";
        } else if (in_right_boundary){
            boundary = "right boundary";
        } else {
            boundary = "no boundary";
        }
        final String direction_string = (direction == Direction.Left)? "left" : "right";
        log("===== Walk " + steps + " steps (distance:" + distance +" to X:" + go_to +") to the " + direction_string + " in " + time + " milliseconds (" + boundary + ")");

        go_to = (go_to < 0)? 0 : go_to;
        go_to = (go_to > BOUNDARIES)? BOUNDARIES : go_to;
        doWalkTo(go_to, time, direction);
    }

    private void doWalkTo(int go_to, int time, Direction direction){
        setDirection(direction);
        view.animate().x(go_to).setDuration(time);
        setPosition(go_to);

        handler = new Handler();
        walking = true;

        final Runnable walk = new Runnable() {
            @Override
            public void run() {
                try {
                    if (isWalking()){
                        view.setImageBitmap(getSprite(getStandingVisual(current_state)));
                    } else {
                        view.setImageBitmap(getSprite(getWalkingVisual(current_state)));
                    }
                } finally {
                    if (walking){
                        handler.postDelayed(this, SPRITE_WALK_TIME);
                    }
                }
            }
        };
        walk.run();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                walking = false;
            }
        }, time);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setImageBitmap(getSprite(getStandingVisual(current_state)));
                next();
            }
        }, time + 300);

    }

    public Visual getStandingVisual(State state){
        Visual visual;
        switch (state){
            case Fat:
                visual = Visual.FatDefault;
                break;
            case Sick:
                visual = Visual.SickDefault;
                break;
            default:
            case Normal:
                visual = Visual.NormalDefault;
                break;
        }
        return visual;
    }

    private Visual getWalkingVisual(State state){
        Visual visual;
        switch (state){
            case Fat:
                visual = Visual.FatWalk;
                break;
            case Sick:
                visual = Visual.SickWalk;
                break;
            default:
            case Normal:
                visual = Visual.NormalWalk;
                break;
        }
        return visual;
    }

    private Visual getEatingVisual(State state){
        Visual visual;
        switch (state){
            case Fat:
                visual = Visual.FatEat;
                break;
            case Sick:
                visual = Visual.SickEat;
                break;
            default:
            case Normal:
                visual = Visual.NormalEat;
                break;
        }
        return visual;
    }

    private boolean isWalking() {
        return (current_visual == Visual.FatWalk || current_visual == Visual.NormalWalk || current_visual == Visual.SickWalk);
    }

    private void doWalkLeft(int steps) {
        doWalk(steps, Direction.Left);
    }

    private void doWalkRight(int steps) {
        doWalk(steps, Direction.Right);
    }

    /*
    ===========================================================================================
    ACTIONS
    ===========================================================================================
    */

    private void addAction(Action action, int value){
        log("Add Action: " + action.toString() + " to List");
        action_counter++;
        actions.add(new SpriteAction(action, value));
    }

    public void walkLeft(int steps){
        addAction(Action.WalkLeft, steps);
    }

    public void walkRight(int steps){
        addAction(Action.WalkRight, steps);
    }

    public void lookOpposite(){
        addAction(Action.LookOpposite, 0);
    }

    public void standStill(){
        addAction(Action.StandStill, 0);
    }

    public void eatFood(){ addAction(Action.EatFood, 400);}

    public void randomAction() {
        int steps = 4;
        steps = steps + new Random().nextInt(4);

        switch (new Random().nextInt(3) + 1){
            case 1:
                walkLeft(steps);
                break;
            case 2:
                walkRight(steps);
                break;
            case 3:
                lookOpposite();
                break;
            default:
            case 4:
                standStill();
                break;
        }
    }

    public static void log(String message){
//        System.out.println("===== SpriteCharacter log: " + message);
//        System.out.println("-");
    }
}

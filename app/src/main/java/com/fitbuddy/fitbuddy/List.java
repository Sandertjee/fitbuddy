package com.fitbuddy.fitbuddy;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Map;

public class List {

    private TaskActivity activity;
    private RelativeLayout overview;
    private LinearLayout main;
    private TextView fitCoinsDisplay;

    List(TaskActivity activity, LinearLayout layout, RelativeLayout container) {
        this.activity = activity;
        main = layout;
        overview = container;
    }


    public void getTitleBar(String title, boolean showPouch) {

        LinearLayout titleBox = new LinearLayout(activity);
        titleBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
        titleBox.setGravity(Gravity.FILL_HORIZONTAL);
        titleBox.setOrientation(LinearLayout.HORIZONTAL);
        titleBox.setPadding(50, 60, 50, 60);
        titleBox.setId(R.id.titleLayout);

        overview.addView(titleBox);

        TextView activityTitle = new TextView(activity);
        activityTitle.setText(title.toUpperCase());
        activityTitle.setTextColor(activity.getColor(R.color.black));
        activityTitle.setTextSize(16);
        activityTitle.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
        activityTitle.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

        titleBox.addView(activityTitle);

        if (showPouch) {
            ImageView fitCoinIcon = new ImageView(activity);
            fitCoinIcon.setLayoutParams(new LinearLayout.LayoutParams(75, 75, 0));
            fitCoinIcon.setPadding(0, 0, 10, 0);
            fitCoinIcon.setImageResource(R.drawable.fitcoin_sprite);

            titleBox.addView(fitCoinIcon);

            fitCoinsDisplay = new TextView(activity);
            fitCoinsDisplay.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            fitCoinsDisplay.setTextColor(activity.getColor(R.color.black));
            fitCoinsDisplay.setTextSize(16);
            fitCoinsDisplay.setTypeface(Typeface.DEFAULT_BOLD);
            fitCoinsDisplay.setText(String.valueOf(activity.getUser().getFitCoins()));
            fitCoinsDisplay.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));
            titleBox.addView(fitCoinsDisplay);
        }
        // else do nothing
    }

    private void buyItem(FoodItem item, FoodItem.ID key) {

        final LinearLayout messagecontainer = activity.findViewById(R.id.LayoutError);
        Button nextStep = activity.findViewById(R.id.btnClose);
        TextView title = activity.findViewById(R.id.TxtTitleDisplay);
        TextView message = activity.findViewById(R.id.txtMessageDisplay);
        LinearLayout selectedItem = activity.findViewById(R.id.layoutSelectedItem);
        TextView itemName = activity.findViewById(R.id.txtItemName);
        ImageView itemIcon = activity.findViewById(R.id.imgIcon);

        title.setTextSize(16);
        title.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

        message.setTextSize(14);
        message.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratregular));

        if (activity.getUser().getFitCoins() - item.getPrice() >= 0) {
            int fitCoin = activity.getUser().getFitCoins() - item.getPrice();
            activity.getUser().insertFitCoins(fitCoin);
            activity.getStorage().save(FitBuddy.COINS_KEY, fitCoin);
            activity.getUser().insertInventory(key);
            updatePouch(fitCoin);

            title.setText("You just Bought :");
            message.setText("Enjoy giving this to your Buddy!");
            nextStep.setText("Close");
            itemName.setText(item.getName());
            itemIcon.setImageBitmap(item.getImage());
            messagecontainer.setVisibility(View.VISIBLE);

            nextStep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messagecontainer.setVisibility(View.INVISIBLE);
                }
            });

        } else {
            title.setText("Oh no...");
            message.setText("Unlock some more achievements to earn more fitcoins");
            nextStep.setText("Close");
            messagecontainer.setVisibility(View.VISIBLE);
            itemIcon.setImageResource(R.drawable.fitcoin_sprite);
            itemIcon.getLayoutParams().width = 110;
            itemIcon.getLayoutParams().height = 110;
            itemName.setText("Not enough FitCoins");
            itemName.setPadding(20, 0, 0, 0);


            nextStep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messagecontainer.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public void updatePouch(){
        updatePouch(activity.getUser().getFitCoins());
    }

    private void updatePouch(int coins) {
        fitCoinsDisplay.setText(String.valueOf(coins));
    }

    public void getAchievementList() {

        getTitleBar("Achievements", false);

        for (final Map.Entry<AchievementItem.ID, AchievementItem> item : AchievementItem.getMap().entrySet()) {

            LinearLayout itemContainer = new LinearLayout(activity);
            itemContainer.setOrientation(LinearLayout.HORIZONTAL);
            itemContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            itemContainer.setPadding(0, 40, 0, 0);
            itemContainer.setGravity(Gravity.CENTER_VERTICAL);

            main.addView(itemContainer);

            ImageView icon = new ImageView(activity);
            icon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            icon.getLayoutParams().height = 130;
            icon.getLayoutParams().width = 130;
            icon.setImageResource(R.drawable.burger);

            itemContainer.addView(icon);

            LinearLayout verticalBox = new LinearLayout(activity);
            verticalBox.setOrientation(LinearLayout.VERTICAL);
            verticalBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            verticalBox.setPadding(45, 0, 0, 0);

            itemContainer.addView(verticalBox);

            LinearLayout textBox = new LinearLayout(activity);
            textBox.setOrientation(LinearLayout.HORIZONTAL);
            textBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 0));
            textBox.setPadding(0, 0, 0, 10);

            verticalBox.addView(textBox);

            TextView title = new TextView(activity);
            title.setText(item.getValue().getName());
            title.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            title.setTextSize(13);
            title.setTypeface(Typeface.DEFAULT_BOLD);
            title.setTextColor(activity.getColor(R.color.black));

            textBox.addView(title);

            TextView progressCount = new TextView(activity);
            progressCount.setText("(" + String.valueOf(item.getValue().getCount()) + " KM)");
            progressCount.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            progressCount.setGravity(Gravity.END);
            progressCount.setTextColor(activity.getColor(R.color.black));
            progressCount.setTextSize(13);
            progressCount.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

            textBox.addView(progressCount);

            ProgressBar progress = new ProgressBar(activity, null, android.R.attr.progressBarStyleHorizontal);
            progress.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            progress.getLayoutParams().height = 30;
            progress.setBackgroundColor(activity.getColor(R.color.gray));
            progress.setMax(100);
            progress.setProgress(item.getValue().getProgress());

            verticalBox.addView(progress);
        }
    }

    public void getStoreList(FoodItem.Categories category, boolean padding) {
        main.setPadding(0, 0, 0, 100);

        LinearLayout categoryTitle = new LinearLayout(activity);
        categoryTitle.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));
        if (padding) {
            categoryTitle.setPadding(0, 70, 0, 20);
        } else {
            categoryTitle.setPadding(0, 0, 0, 20);
        }
        categoryTitle.setOrientation(LinearLayout.HORIZONTAL);

        main.addView(categoryTitle);

        TextView title = new TextView(activity);
        title.setText(category.toString().toUpperCase());
        title.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));
        title.setTextColor(activity.getColor(R.color.orange));
        title.setTextSize(15);
        title.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

        categoryTitle.addView(title);

        for (final Map.Entry<FoodItem.ID, FoodItem> item : FoodItem.getMap().entrySet()) {

            if (item.getValue().getCategory() == category) {

                LinearLayout itemsContainer = new LinearLayout(activity);
                itemsContainer.setOrientation(LinearLayout.HORIZONTAL);
                itemsContainer.setGravity(Gravity.CENTER_VERTICAL);
                itemsContainer.setPadding(0, 30, 0, 15);
                itemsContainer.setTag(item.getKey());
                itemsContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));

                main.addView(itemsContainer);

                itemsContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        buyItem(item.getValue(), item.getKey());
                    }
                });

                ImageView icon = new ImageView(activity);
                icon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));
                icon.getLayoutParams().width = 110;
                icon.getLayoutParams().height = 110;
                icon.setImageBitmap(item.getValue().getImage());

                itemsContainer.addView(icon);

                LinearLayout verticalText = new LinearLayout(activity);
                verticalText.setOrientation(LinearLayout.VERTICAL);
                verticalText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

                itemsContainer.addView(verticalText);

                TextView foodTitle = new TextView(activity);
                foodTitle.setText(item.getValue().getName());
                foodTitle.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                foodTitle.setPadding(20, 0, 0, 0);
                foodTitle.setTextColor(activity.getColor(R.color.black));
                foodTitle.setTextSize(13);
                foodTitle.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

                verticalText.addView(foodTitle);

                TextView foodDescription = new TextView(activity);
                foodDescription.setText(item.getValue().getDescription());
                foodDescription.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                foodDescription.setPadding(20, 0, 0, 0);
                foodDescription.setTextColor(activity.getColor(R.color.gray));
                foodDescription.setTextSize(12);
                foodDescription.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratregular));

                verticalText.addView(foodDescription);

                ImageView coinIcon = new ImageView(activity);
                coinIcon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                coinIcon.getLayoutParams().width = 60;
                coinIcon.getLayoutParams().height = 60;
                coinIcon.setImageResource(R.drawable.fitcoin_sprite);
                coinIcon.setPadding(20, 0, 0, 0);

                itemsContainer.addView(coinIcon);

                TextView foodPrice = new TextView(activity);
                foodPrice.setText(String.valueOf(item.getValue().getPrice()));
                foodPrice.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));
                foodPrice.setPadding(10, 0, 0, 0);
                foodPrice.setTextColor(activity.getColor(R.color.black));
                foodPrice.setTextSize(13);
                foodPrice.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

                itemsContainer.addView(foodPrice);
            }
        }
    }

    public void getInventory() {

        main.setPadding(0, 0, 0, 100);

        for (final Map.Entry<FoodItem.ID, Integer> item : activity.getUser().getInventory().entrySet()) {

            final FoodItem foodItem = FoodItem.get(item.getKey());

            LinearLayout itemsContainer = new LinearLayout(activity);
            itemsContainer.setOrientation(LinearLayout.HORIZONTAL);
            itemsContainer.setGravity(Gravity.CENTER_VERTICAL);
            itemsContainer.setPadding(0, 40, 0, 40);
            itemsContainer.setTag(item.getKey());
            itemsContainer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));

            main.addView(itemsContainer);

            itemsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.getUser().deleteInventory(item.getKey());
                    FitBuddy.setFoodItem(activity, item.getKey());
                    FitBuddy.setHealth(activity, item.getKey());
                    activity.startActivity(new Intent(activity, GameActivity.class));
                }
            });

            ImageView icon = new ImageView(activity);
            icon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.0f));
            icon.getLayoutParams().width = 110;
            icon.getLayoutParams().height = 110;
            icon.setImageBitmap(foodItem.getImage());

            itemsContainer.addView(icon);

            LinearLayout verticalText = new LinearLayout(activity);
            verticalText.setOrientation(LinearLayout.VERTICAL);
            verticalText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));

            itemsContainer.addView(verticalText);

            TextView foodTitle = new TextView(activity);
            foodTitle.setText(foodItem.getName());
            foodTitle.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            foodTitle.setPadding(20, 0, 0, 0);
            foodTitle.setTextColor(activity.getColor(R.color.black));
            foodTitle.setTextSize(13);
            foodTitle.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

            verticalText.addView(foodTitle);

            TextView foodDescription = new TextView(activity);
            foodDescription.setText(foodItem.getDescription());
            foodDescription.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            foodDescription.setPadding(20, 0, 0, 0);
            foodDescription.setTextColor(activity.getColor(R.color.gray));
            foodDescription.setTextSize(12);
            foodDescription.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratregular));

            verticalText.addView(foodDescription);

            TextView itemCountDisplay = new TextView(activity);
            itemCountDisplay.setText(item.getValue().toString() + "x");
            itemCountDisplay.setTextSize(13);
            itemCountDisplay.setTextColor(activity.getColor(R.color.black));
            itemCountDisplay.setTypeface(ResourcesCompat.getFont(activity, R.font.montserratbold));

            itemsContainer.addView(itemCountDisplay);
        }
    }
}



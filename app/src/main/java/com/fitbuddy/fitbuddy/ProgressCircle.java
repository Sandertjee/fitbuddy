package com.fitbuddy.fitbuddy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageButton;

public class ProgressCircle {

    TaskActivity activity;
    ImageButton view;

    ProgressCircle(TaskActivity activity, ImageButton view) {
        this.activity = activity;
        this.view = view;
    }

    public int getProgressResource(double type){
        int resource;
        if (type >= 7){
            resource = R.drawable.progress_7;
        } else if (type > 6){
            resource = R.drawable.progress_6;
        } else if (type > 5){
            resource = R.drawable.progress_5;
        } else if (type > 4){
            resource = R.drawable.progress_4;
        } else if (type > 3){
            resource = R.drawable.progress_3;
        } else if (type > 2){
            resource = R.drawable.progress_2;
        } else if (type > 1){
            resource = R.drawable.progress_1;
        } else {
            resource = R.drawable.progress_1;
        }
        return resource;
    }

    public void update(int percents, int icon_resource, int background_color){
        double type = Math.floor((percents / 100) * 7);
        int resource = getProgressResource(type);
        Bitmap overlay = (BitmapFactory.decodeResource(activity.getResources(), icon_resource));

        view.setBackgroundResource(resource);
        view.setBackgroundTintList(activity.getColorStateList(background_color));
        view.setImageBitmap(overlay);
    }

    public void update(int percents, Bitmap icon_resource, int background_color){
        double type = Math.floor((percents / 100) * 7);
        int resource = getProgressResource(type);

        view.setBackgroundResource(resource);
        view.setBackgroundTintList(activity.getColorStateList(background_color));
        view.setImageBitmap(icon_resource);
    }
}

package com.fitbuddy.fitbuddy;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class StoreActivity extends TaskActivity {

    LinearLayout main;
    RelativeLayout container;
    List store;

    @Override
    protected void onStarted() {
        setContentView(R.layout.activity_store);
        main = findViewById(R.id.vertical_layout);
        container = findViewById(R.id.overview);
        FoodItem.Init(this);

        store = new List(this, main, container);
        store.getTitleBar("Supermarket", true);
    }

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        store.updatePouch();
        int counter = 0;
        for(FoodItem.Categories category : FoodItem.Categories.values()) {
            store.getStoreList(category, (counter > 0));
            counter++;
        }
    }
}

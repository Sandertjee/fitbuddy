package com.fitbuddy.fitbuddy;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TestActivity extends TaskActivity {

    Intent serviceIntent;

    int counter = 0;
    public static final String CHANNEL_ID = "ExampleServiceChannel";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        // move to login so this can never occur?
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            }
        }
        else{
            getUser().logout();
            finishAffinity();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            stopService(serviceIntent);

        }
    }


    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_test);

        GeoLocation.activity = this;

        // if SDK > Oreo, create notification for service;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel serviceChannel = new NotificationChannel(
                  CHANNEL_ID,
                  "Fitbuddy Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }

        // defining geoLocation class
        serviceIntent = new Intent(this, GeoLocation.class);
        serviceIntent.putExtra("inputExtra", "");

        startService(serviceIntent);


        // permissions
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // if we don't have the persmission to use FINE LOCATION

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else{
            // if we already have permission
            startService(serviceIntent);
        }








//        System.out.println(myBuddy);
//        System.out.println(myInventory);
//        System.out.println(myFitCoins);
//
//        myItemList.add(6);
//        myItemList.add(666);
//        myItemList.add(646);
//        myItemList.add(2);

//        TextView something = findViewById(R.id.textView);
        Button inventoryBtn = findViewById(R.id.inventoryBtn);
        Button deleteBtn = findViewById(R.id.deleteBtn);

//
//        something.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                System.out.println(getUser().getEmail());
//                System.out.println("Test:" + getUser().getInventory());
//
////                myFireStoreData.saveData("Bob", 66, myItemList, mAuth, new Runnable() {
////                    @Override
////                    public void run() {
////                        Toast.makeText(GameActivity.this, "Fuck you", Toast.LENGTH_SHORT).show();
////                    }
////                });
////                finish();
////                startActivity(new Intent(GameActivity.this, StoreActivity.class));
//            }
//        });
//
//
        inventoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUser().insertInventory(FoodItem.ID.Kiwi);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUser().deleteInventory(FoodItem.ID.Kiwi);
            }
        });


    }
}

package com.fitbuddy.fitbuddy;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {

    // defining FireStore / FireBase
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    // global names for Firestore field names
    enum Keys {Buddy, FitCoins, Inventory, Length, Weight}

    // standaard waarden
    public static FitBuddy.Character defaultBuddy = FitBuddy.Character.None;
    public static int defaultFitCoins = 0;
    public static double defaultLength = 1.74;
    public static int defaultWeight = 77;
    public static List<String> defaultInventory =  new ArrayList<String>();

    // Define strings
    private String email = mAuth.getCurrentUser().getEmail();
    private Map<FoodItem.ID, Integer> inventory;
    private DocumentReference docRef = db.collection("user-data").document(email);
    private FitBuddy.Character buddy = FitBuddy.Character.None;
    private int fitCoins = defaultFitCoins;
    private double length = defaultLength;
    private int weight = defaultWeight;

    // constructor
    User(){
        inventory = new HashMap();
    }


    // getters
    public Map<FoodItem.ID, Integer> getInventory() {
        return inventory;
    }

    public String getEmail(){
        return email;
    }

    public FitBuddy.Character getBuddy(){
        return buddy;
    }

    public int getFitCoins(){
        return fitCoins;
    }

    public int getWeight() {
        return weight;
    }

    public double getLength() {
        return length;
    }




    // Insert to firestore
    public void insertBuddy(FitBuddy.Character buddy){
        insert(Keys.Buddy, buddy.toString());
        this.buddy = buddy;
    }

    public void insertFitCoins(int fitCoins){
        insert(Keys.FitCoins, fitCoins);
        this.fitCoins = fitCoins;
    }

    public void insertWeight(int weight){
        insert(Keys.Weight, weight);
        this.weight = weight;
    }

    public void insertLength(double length){
        insert(Keys.Length, length);
        this.length = length;
    }

    public void insertInventory(FoodItem.ID food_item){
        List<String> list = new ArrayList<>();
        for(Map.Entry<FoodItem.ID, Integer> item:inventory.entrySet()){
            int amount = item.getValue();
            if (item.getKey() == food_item){
                amount++;
            }
            item.setValue(amount);

            list.add(item.getKey().toString() + ":" + amount);
        }

        if(!inventory.containsKey(food_item)){
            inventory.put(food_item, 1);
            list.add(food_item.toString() + ":" + 1);
        }

        // Update Inventory array in Firestore

        insert(Keys.Inventory, list);
    }

    public void deleteInventory(FoodItem.ID food_item){
        FoodItem.ID sel_Key = null;
        List<String> list = new ArrayList<>();

        for(Map.Entry<FoodItem.ID, Integer> item:inventory.entrySet()){
            int amount = item.getValue();
            if(item.getKey() == food_item){
                amount--;
            }

            // if amount of the item is 0, set sel_Key to the food_item key
            if(item.getKey() == food_item && amount == 0){
                sel_Key = item.getKey();
            }

            item.setValue(amount);
            // if there is more then 1 item, add to the list
            if(amount > 0){
                list.add(item.getKey().toString() + ":" + amount);
            }
        }

        // if the item has 0 amounts, remove from the map
        if(sel_Key != null){
            System.out.println(sel_Key);
            inventory.remove(sel_Key);
        }

        // update inventory array in Firestore
        insert(Keys.Inventory, list);
    }




    // global inserts to FireStore
    private void insert(Keys key, int value){
        docRef.update(key.toString(), value);
    }

    private void insert(Keys key, String value){
        docRef.update(key.toString(), value);
    }

    private void insert(Keys key, double value){
        docRef.update(key.toString(), value);
    }

    private void insert(Keys key, List value){
        docRef.update(key.toString(), value);
    }




    // logout user;
    public void logout(){
        mAuth.signOut();
    }


    // get data from Firebase - If data not there insert standard values
    public void load(){
        load(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    public void load(final Runnable runnable){
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        JSONObject c = new JSONObject(document.getData());
                        try{
                            buddy = FitBuddy.Character.valueOf(c.getString(Keys.Buddy.toString()));
                            fitCoins = c.getInt(Keys.FitCoins.toString());
                            length = c.getDouble(Keys.Length.toString());
                            weight = c.getInt(Keys.Weight.toString());

                            JSONArray myJSONArray = c.getJSONArray("Inventory");

                            for(int i = 0; i < myJSONArray.length(); i++ ){
                                String[] JSON = myJSONArray.getString(i).split(":");
                                try {
                                    inventory.put(FoodItem.ID.valueOf(JSON[0]), Integer.valueOf(JSON[1]));
                                }
                                catch(Exception e){
                                    System.out.println("fooditem " + JSON[0] + " Not recognised!");
                                }
                            }

                            System.out.println("Fetched buddy: " + buddy);
                            System.out.println("Fetched FitCoins: " + fitCoins);
                            System.out.println("Fetched Length: " + length);
                            System.out.println("Fetched Weight: " + weight);
                            System.out.println("Fetched Inventory: " + inventory);

                            runnable.run();
                        }
                        catch(JSONException e){
                            System.out.println("Caught exception, document existed");
                            System.out.println(e);
                            resetData(new Runnable() {
                                @Override
                                public void run() {
                                    load(runnable);
                                }
                            });
                        }
                    } else {
                        System.out.println("No data to be fetched");
                        resetData(new Runnable() {
                            @Override
                            public void run() {
                                load(runnable);
                            }
                        });
                    }
                } else {
                    System.out.println("System Failure");
                }
            }
        });
    }

    public void saveData(String buddy, int fitCoins, List inventory, double length, int weight, FirebaseAuth mAut){
        saveData(buddy, fitCoins, inventory, length, weight, mAut, new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    // Saves all the data to the firestore database
    public void saveData(String buddy, int fitCoins, List inventory, double length, int weight, FirebaseAuth mAuth, final Runnable runable){
        Map<String, Object> user = new HashMap<>();
        user.put(Keys.Buddy.toString(), buddy);
        user.put(Keys.FitCoins.toString(), fitCoins);
        user.put(Keys.Inventory.toString(), inventory);
        user.put(Keys.Length.toString(), length);
        user.put(Keys.Weight.toString(), weight);

        db.collection("user-data").document(mAuth.getCurrentUser().getEmail())
        .set(user)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("Success");
                runable.run();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                System.out.println("Failure");
            }
        });
    }

    private void resetData(Runnable runnable){
        System.out.println("No data to be fetched");
        saveData(defaultBuddy.toString(), defaultFitCoins, defaultInventory, defaultLength, defaultWeight , mAuth, runnable);
    }


}

package com.fitbuddy.fitbuddy;

import java.util.HashMap;
import java.util.Map;

public class AchievementItem {

    private static Map<ID, AchievementItem> achievementList;

    private String achievementName;
    private int achievementIcon;
    private double achievementProgressCount;
    private int achievementProgress;
    private Categories category;

    enum ID {
        fiveKilometers,
        tenKilometers,
        fifteenKilometers
    }

    enum Categories {
        Movement,
        Feeding,
        Alive
    }

    private AchievementItem(String name, int icon, double kilometers, int progress, Categories categories) {
        achievementName = name;
        achievementIcon = icon;
        achievementProgressCount = kilometers;
        achievementProgress = progress;
        category = categories;
    }

    public static void init() {

        achievementList = new HashMap<>();

        achievementList.put(ID.fiveKilometers, new AchievementItem("Walk 5 kilometers", R.drawable.redbull, 2.5, 50, Categories.Movement));
        achievementList.put(ID.tenKilometers, new AchievementItem("Walk 10 kilometers", R.drawable.redbull, 5, 50, Categories.Movement));
        achievementList.put(ID.fifteenKilometers, new AchievementItem("Walk 15 kilometers", R.drawable.redbull, 10, 65, Categories.Movement));
    }

    public static AchievementItem get(ID id) {
        return achievementList.get(id);
    }

    public String getName() {
        return achievementName;
    }

    public int getIcon() {
        return achievementIcon;
    }

    public double getCount() {
        return achievementProgressCount;
    }

    public int getProgress() {
        return achievementProgress;
    }

    public static Map<ID, AchievementItem> getMap() {
        return achievementList;
    }

}

package com.fitbuddy.fitbuddy;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class Storage {

    enum FileType {SETTINGS, PLAYER, GAME}

    private static final String SETTINGS_FILE = "settings_file";
    private static final String GAME_FILE = "game_file";
    private static final String PLAYER_FILE = "player_file";
    private static final String FITBUDDY_FILE = "fitbuddy";

    private SharedPreferences data;
    private SharedPreferences.Editor data_editor;


    /**
     *
     * @param activity
     */
    Storage(Activity activity, FileType file_name){
        openSharedPreferences(activity, getFile(file_name));
    }

    Storage(Activity activity, String file_name){
        openSharedPreferences(activity, file_name);
    }

    Storage(Activity activity){ openSharedPreferences(activity, FITBUDDY_FILE);}

    private void openSharedPreferences(Activity activity, String file_name){
        data = activity.getSharedPreferences(file_name, MODE_PRIVATE);
        data_editor = data.edit();
    }

    /**
     * Converts Enum FileType to specified file
     * @param file
     * @return String
     */
    private String getFile(FileType file){
        switch (file){
            default:
            case SETTINGS:  return SETTINGS_FILE;
            case PLAYER:    return PLAYER_FILE;
            case GAME:      return GAME_FILE;
        }
    }

    void save(String key, String value){    data_editor.putString(key, value).apply(); }
    void save(String key, Integer value){   data_editor.putInt(key, value).apply(); }
    void save(String key, Boolean value){   data_editor.putBoolean(key, value).apply(); }
    void save(String key, Float value){     data_editor.putFloat(key, value).apply(); }
    void save(String key, Long value){     data_editor.putLong(key, value).apply(); }
    void save(String key, Double value){     data_editor.putInt(key, (int) Math.ceil(value)).apply(); }

    String  load(String key, String else_return){  return data.getString(key, else_return); }
    Integer load(String key, Integer else_return){ return data.getInt(key, else_return); }
    Boolean load(String key, Boolean else_return){ return data.getBoolean(key, else_return); }
    Float   load(String key, Float else_return){   return data.getFloat(key, else_return); }
    Long    load(String key, Long else_return){   return data.getLong(key, else_return); }

    /**
     * Empties the specified settings file
     * @return
     */
    boolean clear(){
        try {
            data_editor.clear();
            data_editor.commit();
            return true;
        } catch (Exception e){
            Log.e("Settings", e.toString());
            return false;
        }
    }
}

package com.fitbuddy.fitbuddy;

public class SpriteAction {

    private SpriteCharacter.Action action;
    private int value;

    SpriteAction(SpriteCharacter.Action action, int value){
        this.action = action;
        this.value = value;
    }

    public SpriteCharacter.Action getSpriteAction(){ return action; }

    public int getValue(){ return value; }

}

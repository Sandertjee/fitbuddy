package com.fitbuddy.fitbuddy;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import javax.annotation.Nullable;

import static com.fitbuddy.fitbuddy.TestActivity.CHANNEL_ID;

public class GeoLocation extends Service {

    LocationManager locationManager;
    LocationListener locationListener;

    public static TaskActivity activity;

    boolean use_location = false;

    int counter = -1;

    @Override
    public void onCreate(){
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        String input = intent.getStringExtra("inputExtra");

        Intent notificationIntent = new Intent(this, GameActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Fitbuddy is active and tracking your movement!")
                .setContentText(input)
                .setSmallIcon(R.drawable.fitbuddy_f)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_LOW)
                .build();

        startForeground(1, notification);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (use_location){
                    Log.i("Location4", location.toString());

                    if(counter > 0){
                        activity.getUser().insertFitCoins(activity.getUser().getFitCoins() + 2);
                        activity.getStorage().save(FitBuddy.COINS_KEY, activity.getUser().getFitCoins() + 2);
                        activity.getStorage().save(FitBuddy.HEALTH_KEY, activity.getStorage().load(FitBuddy.HEALTH_KEY, 500) - 3);
                        activity.getStorage().save(FitBuddy.WALKED_KEY, activity.getStorage().load(FitBuddy.WALKED_KEY, 0) + 100);

                        System.out.println("COINS:" + activity.getStorage().load(FitBuddy.COINS_KEY, 3000));
                        System.out.println("HEALTH: " +activity.getStorage().load(FitBuddy.HEALTH_KEY, 500));

                    }
                    counter++;
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 100, locationListener);
        }
        catch(SecurityException e){
            System.out.println(e);
        }


        return START_NOT_STICKY;
    }

    public void onDestroy(){
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

}

package com.fitbuddy.fitbuddy;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Map;

public class AchievementsActivity extends TaskActivity {

    LinearLayout main;
    RelativeLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements);

        AchievementItem.init();

        main = findViewById(R.id.vertical_layout);
        container = findViewById(R.id.overview);

        loadList();
    }

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {

    }

    public void loadList() {
        List achievements = new List(this, main, container);
        achievements.getAchievementList();
    }
}

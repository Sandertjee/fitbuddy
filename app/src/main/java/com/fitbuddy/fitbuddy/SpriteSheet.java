package com.fitbuddy.fitbuddy;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;

public class SpriteSheet {

    protected Bitmap image;

    private int rows;
    private int columns;

    private int width;
    private int height;

    private int image_width;
    private int image_height;

    SpriteSheet(Activity activity, int resource, int rows, int columns){
        this(BitmapFactory.decodeResource(activity.getResources(), resource), rows, columns);
    }

    SpriteSheet(Bitmap image, int rows, int columns){
        this.image = image;
        this.rows = rows;
        this.columns = columns;

        this.image_width = image.getWidth();
        this.image_height = image.getHeight();

        this.width = image_width / columns;
        this.height = image_height / rows;
    }

    public int getWidth(){ return width; }
    public int getHeight(){ return height; }

    public Bitmap getSprite(int row, int col){
        return Bitmap.createBitmap(image, ((col - 1) * width), ((row - 1) * height), width, height);
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, (bmp1.getHeight() / 2) - bmp2.getHeight() / 2, null);
        return bmOverlay;
    }

    public void clear(){
        image.recycle();
    }
}

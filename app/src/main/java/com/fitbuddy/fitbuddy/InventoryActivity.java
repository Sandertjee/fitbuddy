package com.fitbuddy.fitbuddy;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class InventoryActivity extends TaskActivity {

    LinearLayout main;
    RelativeLayout container;
    List list;

    @Override
    protected void onStarted() {
        setContentView(R.layout.activity_inventory);
        main = findViewById(R.id.vertical_layout);
        container = findViewById(R.id.overview);

        list = new List(this, main, container);
        list.getTitleBar("Inventory", true);

        FoodItem.Init(this);
    }

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        list.updatePouch();
        list.getInventory();
    }

}

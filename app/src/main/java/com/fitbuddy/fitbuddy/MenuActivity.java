package com.fitbuddy.fitbuddy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MenuActivity extends TaskActivity implements View.OnClickListener, View.OnLongClickListener {

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
    }

    @Override
    protected void onStarted() {
        setContentView(R.layout.activity_menu);

        ImageButton storeBtn = findViewById(R.id.storeBtn);
        storeBtn.setOnClickListener(this);

        ImageButton achievementBtn = findViewById(R.id.achievementBtn);
        achievementBtn.setOnClickListener(this);

        ImageButton settingsBtn = findViewById(R.id.settingsBtn);
        settingsBtn.setOnClickListener(this);
        settingsBtn.setOnLongClickListener(this);

        ImageButton inventoryBtn = findViewById(R.id.inventoryBtn);
        inventoryBtn.setOnClickListener(this);
        inventoryBtn.setOnLongClickListener(this);

        ImageButton backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.achievementBtn:
                startActivity(new Intent(this, AchievementsActivity.class));
                break;
            case R.id.storeBtn:
                startActivity(new Intent(this, StoreActivity.class));
                break;
            case R.id.inventoryBtn:
                startActivity(new Intent(this, InventoryActivity.class));
                break;
            case R.id.settingsBtn:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()){
            case R.id.settingsBtn:
                startActivity(new Intent(this, TestActivity.class));
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

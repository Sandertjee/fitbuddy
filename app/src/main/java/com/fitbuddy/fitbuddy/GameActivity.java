package com.fitbuddy.fitbuddy;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GameActivity extends TaskActivity implements View.OnClickListener, View.OnLongClickListener {

    FitBuddy buddy;
    SpriteCharacter character;
    ImageView sprite;

    RelativeLayout background;
    ImageButton menuBtn;

    TextView deathText;
    Button deathBtn;

    User user;
    Storage storage;
    Handler handler;
    MediaPlayer player;

    TextView fitCoinsTxt;
    TextView spriteName;
    ImageView fitCoinsImg;

    ImageButton healthView;
    TextView healthTxt;
    ProgressCircle healthBar;

    ImageButton exerciseView;
    TextView exerciseTxt;
    ProgressCircle exerciseBar;

    boolean death_prompt = false;

    boolean game_open = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        FoodItem.Init(this);
        game_open = true;

        storage = getStorage();
        player = MediaPlayer.create(this, R.raw.sad_violin);

        handler = new Handler();

        sprite = findViewById(R.id.sprite);
        sprite.setVisibility(View.INVISIBLE);

        background = findViewById(R.id.background);

        menuBtn = findViewById(R.id.menuBtn);
        menuBtn.setOnClickListener(this);

        deathText = findViewById(R.id.deathText);
        deathText.setAlpha(0f);

        deathBtn = findViewById(R.id.deathBtn);
        deathBtn.setAlpha(0f);
        deathBtn.setOnClickListener(this);

        fitCoinsTxt = findViewById(R.id.fitCoinsTxt);
        fitCoinsTxt.setVisibility(View.VISIBLE);

        fitCoinsImg = findViewById(R.id.fitCoinsImg);
        fitCoinsImg.setVisibility(View.VISIBLE);

        spriteName = findViewById(R.id.spriteName);
        spriteName.setVisibility(View.VISIBLE);

        healthView = findViewById(R.id.healthView);
        healthTxt = findViewById(R.id.healthTxt);
        healthBar = new ProgressCircle(this, healthView);

        exerciseView = findViewById(R.id.exerciseView);
        exerciseTxt = findViewById(R.id.exerciseTxt);
        exerciseBar = new ProgressCircle(this, exerciseView);

        System.out.println("Your current health: " + getStorage().load(FitBuddy.HEALTH_KEY, 9001));
    }

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        user = getUser();
        updateLocalValues(user.getWeight(), user.getLength(), user.getBuddy(), user.getFitCoins());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (game_open){
                    updateScreen(getUser().getFitCoins(), buddy.getBuddy(), buddy.getHealthPercentage(storage.load(FitBuddy.HEALTH_KEY, 0)), (storage.load(FitBuddy.WALKED_KEY, 0) / 5000) * 100);
                    handler.postDelayed(this, 10000);
                    System.out.println("======= Update screen");
                }
            }
        }, 0);
    }

    @Override
    protected void onStarted() {
        game_open = true;
        buddy = new FitBuddy(this);
        character = buddy.getCharacter(sprite, buddy.isDead());
        if (buddy.isDead() || buddy.getBuddy() == FitBuddy.Character.None) {
            onCharacterDeath();
        } else {
            character.standStill();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (character.isRunning() && game_open) {
                        character.randomAction();
                        handler.postDelayed(this, 2000);
                    }
                }
            }, 0);
            sprite.setOnLongClickListener(this);
            FitBuddy.setFoodItem(this, FoodItem.ID.None);
        }
    }

    // TODO: Check states of Buddy for going in and out of Menu activity

    @Override
    protected void onResume() {
        super.onResume();
        game_open = true;
        character.resume();
        sprite.setVisibility(View.VISIBLE);
        updateScreen(
                getStorage().load(FitBuddy.COINS_KEY, getUser().getFitCoins()),
                FitBuddy.Character.valueOf(getStorage().load(FitBuddy.BUDDY_KEY, getUser().getBuddy().toString())),
                buddy.getHealthPercentage(storage.load(FitBuddy.HEALTH_KEY, 500)),
                (storage.load(FitBuddy.WALKED_KEY, 0) / 5000) * 100
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        game_open = false;
        character.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        game_open = false;
        character.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        game_open = false;
        character.kill();
        player.stop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn:
                startActivity(new Intent(GameActivity.this, MenuActivity.class));
                break;
            case R.id.deathBtn:
                if (death_prompt){
                    FitBuddy.jump(this, SelectionActivity.class);
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.sprite:
                if (!buddy.isDead()) {
                    buddy.isDead(true);
                    onCharacterDeath();
                } else {
                    System.out.println("=== CANNOT KILL " + buddy.getName().toUpperCase() + " ===");
                }
                break;
        }
        return false;
    }

    private void updateLocalValues(int weight, double length, FitBuddy.Character buddy, int coins) {
        storage.save(FitBuddy.WEIGHT_KEY, weight);
        storage.save(FitBuddy.LENGTH_KEY, length);
        storage.save(FitBuddy.BUDDY_KEY, buddy.toString());
        storage.save(FitBuddy.COINS_KEY, coins);
        System.out.println("====== New local data saved");
        System.out.println("Local health: " + storage.load(FitBuddy.HEALTH_KEY, 0));
        System.out.println("Local coins: " + storage.load(FitBuddy.COINS_KEY, 0));
        System.out.println("Local buddy: " + storage.load(FitBuddy.BUDDY_KEY, FitBuddy.Character.None.toString()));
        System.out.println("Health percentage: " + this.buddy.getHealthPercentage());
    }

    protected void updateScreen(int coins, FitBuddy.Character character, int health_percentage, int exercise_percentage) {
        System.out.println("Update screen - COINS: " + coins);
        System.out.println("Update screen - CHARACTER: " + character);
        System.out.println("Update screen - HEALTH %: " + health_percentage);
        System.out.println("Update screen - HEALTH: " + storage.load(FitBuddy.HEALTH_KEY, 0));
        System.out.println("Update screen - EXERCISE &: " + exercise_percentage);

        fitCoinsTxt.setText(String.valueOf(coins));
        spriteName.setText(character.toString());
        healthBar.update(health_percentage, BitmapFactory.decodeResource(getResources(), R.drawable.heart_icon), R.color.red_light);
        healthTxt.setText(String.valueOf(health_percentage) + "HP");

        exerciseBar.update(exercise_percentage, BitmapFactory.decodeResource(getResources(), R.drawable.icon_map), R.color.blue_light);
        exerciseTxt.setText(String.valueOf(exercise_percentage) + "M");
    }

    protected void onCharacterDeath() {
        spriteName.setVisibility(View.INVISIBLE);
        fitCoinsTxt.setVisibility(View.INVISIBLE);
        fitCoinsImg.setVisibility(View.INVISIBLE);
        healthView.setVisibility(View.INVISIBLE);
        healthTxt.setVisibility(View.INVISIBLE);
        exerciseView.setVisibility(View.INVISIBLE);
        exerciseTxt.setVisibility(View.INVISIBLE);
        player.stop();
        player = MediaPlayer.create(this, R.raw.sad_violin);
        FitBuddy.setFoodItem(this, FoodItem.ID.None);
        setTheme(R.style.DeathScreen);
        menuBtn.setVisibility(View.INVISIBLE);
        character.isDead(true);
        character.stop();
        character.spawnMiddle(true);

        System.out.println("======== The Death of " + buddy.getName() + " ========");
        sprite.setImageBitmap(FitBuddy.getCharacterImage(this, buddy.getBuddy(), SpriteCharacter.State.Dead));


        System.out.println("=== Last health: " + storage.load(FitBuddy.HEALTH_KEY, 0));
        System.out.println("=== Last coins: " + storage.load(FitBuddy.COINS_KEY, 0));
        System.out.println("=== Last buddy: " + storage.load(FitBuddy.BUDDY_KEY, FitBuddy.Character.None.toString()));

        sprite.setAlpha(0f);

        background.setBackgroundColor(getColor(R.color.black));

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getColor(R.color.black));

        deathText.setText(character.getDeathSentence());
        int TIME = 1500;
        int EXTRA_TIME = 9000;

        if (buddy.getBuddy() != FitBuddy.Character.None) {
            EXTRA_TIME = player.getDuration();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    character.died();
                    System.out.println("Show");
                    sprite.animate().alpha(1f).setDuration(700);
                    sprite.animate().y((float) -character.getHeight() - 400).setDuration(20000);
                    player.start();
                }
            }, TIME);

            TIME = TIME + 500;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    deathText.animate().alpha(1f).setDuration(1000);
                    deathText.animate().y((float) ((character.getScreenHeight() / 6) * 3.5)).setDuration(1000);
                }
            }, TIME);

            TIME = TIME + 13000;
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    deathText.animate().alpha(1f).setDuration(1000);
                    player.start();
                }
            }, TIME);
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                deathText.animate().y((float) ((character.getScreenHeight() / 3))).setDuration(1000);
                deathBtn.animate().y((character.getScreenHeight() / 3) * 2).setDuration(1000);
            }
        }, TIME);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                death_prompt = true;
                deathBtn.animate().alpha(1f).setDuration(1000);
                getStorage().save(FitBuddy.BUDDY_KEY, FitBuddy.Character.None.toString());
                getUser().insertBuddy(FitBuddy.Character.None);
                System.out.println("======== Reset character ========");
            }
        }, EXTRA_TIME - 5000);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                player.stop();
            }
        }, player.getDuration() + 5000);
    }
}
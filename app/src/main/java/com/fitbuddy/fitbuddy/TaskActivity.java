package com.fitbuddy.fitbuddy;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

abstract class TaskActivity extends AppCompatActivity {

    private User user;
    private Storage storage;
    private Bundle savedInstanceState;

    Intent serviceIntent;
    public static final String CHANNEL_ID = "ExampleServiceChannel";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;

        // create new user
        user = new User();
        storage = new Storage(this);


        if(serviceIntent == null){
            GeoLocation.activity = this;

            // if SDK > Oreo, create notification for service;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                NotificationChannel serviceChannel = new NotificationChannel(
                        CHANNEL_ID,
                        "Fitbuddy Service Channel",
                        NotificationManager.IMPORTANCE_DEFAULT
                );
                NotificationManager manager = getSystemService(NotificationManager.class);
                manager.createNotificationChannel(serviceChannel);
            }

            // defining geoLocation class
            serviceIntent = new Intent(this, GeoLocation.class);
            serviceIntent.putExtra("inputExtra", "");

            startService(serviceIntent);
        }

        // permissions
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // if we don't have the persmission to use FINE LOCATION

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else{
            // if we already have permission
            startService(serviceIntent);
        }

    }



    // if user clicks the allow or deny button
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            }
        }
        else{
            getUser().logout();
            finishAffinity();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
            stopService(serviceIntent);

        }
    }




    @Override
    protected void onStart() {
        super.onStart();
        user.load(new Runnable() {
            @Override
            public void run() {
                // start instance when data has been loaded
                start(savedInstanceState);
            }
        });
        onStarted();
    }

    protected void onStarted(){}

    // Start only when user is logged in
    protected void start(Bundle savedInstanceState){
        if(user.getEmail() != null){
            onTaskCreate(savedInstanceState);
        }
        else{
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }


    // every child of this activity needs OnTaskCreate
    protected abstract void onTaskCreate(Bundle savedInstanceState);

    protected User getUser(){
        return user;
    }

    protected Storage getStorage(){
        return storage;
    }



}

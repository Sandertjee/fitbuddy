package com.fitbuddy.fitbuddy;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class SelectionActivity extends TaskActivity implements View.OnClickListener {

    ImageButton sprite1;
    ImageButton sprite2;
    ImageButton sprite3;
    ImageButton sprite4;
    ImageButton sprite5;
    ImageButton sprite6;

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_selection);
        setButton(sprite1, R.id.sprite1, FitBuddy.Character.Bill);
        setButton(sprite2, R.id.sprite2, FitBuddy.Character.Janice);
        setButton(sprite3, R.id.sprite3, FitBuddy.Character.John);
        setButton(sprite4, R.id.sprite4, FitBuddy.Character.Carrie);
        setButton(sprite5, R.id.sprite5, FitBuddy.Character.Samuel);
        setButton(sprite6, R.id.sprite6, FitBuddy.Character.Sarah);
    }

    private void setButton(ImageView view, int res_id, FitBuddy.Character character) {
        view = findViewById(res_id);
        view.setOnClickListener(this);
        Bitmap bitmap = FitBuddy.getCharacterImage(this, character);
        int width = (int) (bitmap.getWidth() * 0.7);
        int height = (int) (bitmap.getHeight() * 0.7);
        view.setImageBitmap(Bitmap.createScaledBitmap(bitmap, width, height, false));
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.sprite1:
                chooseSprite(FitBuddy.Character.Bill);
                break;
            case R.id.sprite2:
                chooseSprite(FitBuddy.Character.Janice);
                break;
            case R.id.sprite3:
                chooseSprite(FitBuddy.Character.John);
                break;
            case R.id.sprite4:
                chooseSprite(FitBuddy.Character.Carrie);
                break;
            case R.id.sprite5:
                chooseSprite(FitBuddy.Character.Samuel);
                break;
            case R.id.sprite6:
                chooseSprite(FitBuddy.Character.Sarah);
                break;
        }
    }

    private void chooseSprite(FitBuddy.Character character) {
        getUser().insertBuddy(character);
        getStorage().save(FitBuddy.BUDDY_KEY, character.toString());
        getStorage().save(FitBuddy.HEALTH_KEY, 500);
        Toast.makeText(this, "You've chosen " + character.toString(), Toast.LENGTH_LONG).show();
        FitBuddy.jump(this, GameActivity.class);
    }
}

package com.fitbuddy.fitbuddy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class FitBuddy {

    public static String HAND_KEY = "fitbuddy_hand";
    public static String BUDDY_KEY = "fitbuddy_character";
    public static String WEIGHT_KEY = "fitbuddy_weight";
    public static String LENGTH_KEY = "fitbuddy_length";
    public static String COINS_KEY = "fitbuddy_fitcoins";
    public static String STATE_KEY = "fitbuddy_state";
    public static String HEALTH_KEY = "fitbuddy_health";
    public static String WALKED_KEY = "fitbuddy_walked";
    public static String WALKED_PER_DAY_KEY = "fitbuddy_walked_day";

    enum Character {None, Ruud, Samuel, Carrie, John, Bill, Sarah, Janice}

    enum BodyState {UnderWeight, Normal, Overweight}

    private TaskActivity activity;
    private BodyState body_state;
    private int health;
    private boolean is_dead = false;


    private Character buddy;
    private String name = "";
    private FoodItem.ID current_food_item = FoodItem.ID.None;

    FitBuddy(TaskActivity activity) {
        this.activity = activity;
        User user = activity.getUser();

        if (activity.getStorage().load(HEALTH_KEY, 9999) == 9999) {
            activity.getStorage().save(HEALTH_KEY, 500);
            this.health = 500;
        } else {
            this.health = activity.getStorage().load(HEALTH_KEY, 500);
        }

        if (user != null) {
            this.buddy = FitBuddy.Character.valueOf(activity.getStorage().load(BUDDY_KEY, user.getBuddy().toString()));
        } else {
            this.buddy = FitBuddy.Character.valueOf(activity.getStorage().load(BUDDY_KEY, Character.None.toString()));
        }
        this.name = buddy.toString();

        log("Current health: " + health);
        if (health >= 950 || health <= 50) {
            is_dead = true;
        }


        if (health < 300) {
            body_state = BodyState.UnderWeight;
        } else if (health > 700) {
            body_state = BodyState.Overweight;
        } else {
            body_state = BodyState.Normal;
        }
        try {
            current_food_item = FoodItem.ID.valueOf(activity.getStorage().load(HAND_KEY, FoodItem.ID.None.toString()));
        } catch (Exception e) {
            log(e.toString());
        }
    }

    public boolean isDead() {
        return is_dead;
    }

    public void isDead(boolean bool) {
        is_dead = bool;
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public int getHealthPercentage(){
        return getHealthPercentage(String.valueOf(health));
    }

    public int getHealthPercentage(int current_health_string){
        return getHealthPercentage(String.valueOf(current_health_string));
    }

    public int getHealthPercentage(String current_health_string){
        double current_health = Double.parseDouble(current_health_string);
        if (body_state == BodyState.Overweight){
            current_health = current_health - 500;
            current_health = 500 - current_health;
        }
        return (int)(Math.floor(current_health / 500) * 100);
    }

    public BodyState getBodyState(){
        return body_state;
    }

    public Character getBuddy() {
        return buddy;
    }

    public SpriteCharacter getCharacter(ImageView view, boolean dead) {
        SpriteCharacter.State state;
        switch (body_state) {
            case UnderWeight:
                state = SpriteCharacter.State.Sick;
                break;
            default:
            case Normal:
                state = SpriteCharacter.State.Normal;
                break;
            case Overweight:
                state = SpriteCharacter.State.Fat;
                break;
        }
        return getSpriteCharacter(activity, view, buddy, state, dead);
    }

    private SpriteCharacter getSpriteCharacter(TaskActivity activity, ImageView view, Character character, SpriteCharacter.State state, boolean dead) {
        log("Current character: " + character);
        return new SpriteCharacter(activity, view, getCharacterSheet(character), character.toString(), state, current_food_item, !dead);
    }

    public static Bitmap getCharacterImage(TaskActivity activity, Character character) {
        return getCharacterImage(activity, character, SpriteCharacter.State.Normal);
    }

    public static Bitmap getCharacterImage(TaskActivity activity, Character character, SpriteCharacter.State state) {
        SpriteCharacter sprite_character = new SpriteCharacter(activity, getCharacterSheet(character), character.toString());
        sprite_character.setState(state);
        return sprite_character.getSprite(sprite_character.getStandingVisual(state));
    }

    private static int getCharacterSheet(Character character) {
        log("Spritesheet: " + character);
        int sheet;
        switch (character) {
            default:
            case Ruud:
                sheet = R.drawable.ruud_spritesheet;
                break;
            case Samuel:
                sheet = R.drawable.samuel_spritesheet;
                break;
            case Janice:
                sheet = R.drawable.janice_spritesheet;
                break;
            case John:
                sheet = R.drawable.john_spritesheet;
                break;
            case Carrie:
                sheet = R.drawable.carrie_spritesheet;
                break;
            case Sarah:
                sheet = R.drawable.sarah_spritesheet;
                break;
            case Bill:
                sheet = R.drawable.bill_spritesheet;
                break;
        }
        return sheet;
    }


    public static void setFoodItem(TaskActivity activity, FoodItem.ID food_item) {
        activity.getStorage().save(FitBuddy.HAND_KEY, food_item.toString());
    }

    public static void setHealth(TaskActivity activity, FoodItem.ID food_item) {
        FoodItem fooditem = FoodItem.get(food_item);
        activity.getStorage().save(HEALTH_KEY, activity.getStorage().load(HEALTH_KEY, 9001) - fooditem.getHealthPoints());
    }

    public static FoodItem.ID getFoodItem(TaskActivity activity) {
        return FoodItem.ID.valueOf(activity.getStorage().load(HAND_KEY, FoodItem.ID.None.toString()));
    }

    public static void backToMenu(Activity activity) {
        jump(activity, MenuActivity.class);
    }

    public static void jump(Activity activity, Class<?> to) {
        activity.finishAffinity();
        Intent intent = new Intent(activity, to);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        activity.startActivity(intent);
    }

    public static void log(String message){
//        System.out.println("===== SpriteCharacter log: " + message);
//        System.out.println("-");
    }
}

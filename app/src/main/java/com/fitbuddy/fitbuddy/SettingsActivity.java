package com.fitbuddy.fitbuddy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SettingsActivity extends TaskActivity implements View.OnClickListener {

    Button logoutBtn;

    @Override
    protected void onTaskCreate(Bundle savedInstanceState) {
        logoutBtn.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStarted() {
        setContentView(R.layout.activity_settings);

        logoutBtn = findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(this);
        logoutBtn.setVisibility(View.INVISIBLE);

        ImageButton backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.logoutBtn:
                logoutUser();
                break;

            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private void logoutUser() {
        getUser().logout();
        finishAffinity();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
